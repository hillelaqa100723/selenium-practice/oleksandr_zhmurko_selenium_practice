package org.zhmurko.tests.hw14;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.AbstractSet;
import java.util.Set;

public class WindowsTest {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("https://the-internet.herokuapp.com/windows");
    }

    @Test
    public void windowsTest() throws InterruptedException {
        String mainWindowHandle = driver.getWindowHandle();
        driver.findElement(By.linkText("Click Here")).click();

        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle : windowHandles) {
            if (!handle.equals(mainWindowHandle)) {
                driver.switchTo().window(handle);
                break;
            }
        }
        Thread.sleep(3000);

        String text = driver.findElement(By.xpath("//h3")).getText();
        String currentUrl = driver.getCurrentUrl();


        Assert.assertEquals(text, "New Window");
        Assert.assertTrue(currentUrl.endsWith("/new"));

        driver.close();

        driver.switchTo().window(mainWindowHandle);

        text = driver.findElement(By.xpath("//h3")).getText();

        Assert.assertEquals(text, "Opening a new window");
        Assert.assertTrue(driver.findElement(By.linkText("Click Here")).isDisplayed());
    }
}


