package org.zhmurko.tests.hw16;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DownloadFile {
    WebDriver driver = new FirefoxDriver();
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

    @DataProvider(name = "data")
    public Object[][] testData() {
        return new Object[][]{
                {"4", "Paragraphs"},
                {"250", "Words"},
                {"50", "Sentences"}
        };
    }


    @Test(dataProvider = "data")
    public void downloadTest(String amount, String type) throws  IOException {
        driver.get("https://www.webfx.com/tools/lorem-ipsum-generator/");

        WebElement amountField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("amount_generator")));
        amountField.clear();
        amountField.sendKeys(amount);

        Select typeField = new Select(driver.findElement(By.name("type")));
        typeField.selectByVisibleText(type);

        WebElement generateButton = driver.findElement(By.tagName("button"));
        generateButton.click();

        String text = driver.findElement(By.name("result_field")).getText();

        driver.get("https://demo.seleniumeasy.com/generate-file-to-download-demo.html");

        WebElement pasteText = driver.findElement(By.id("textbox"));
        pasteText.sendKeys(text);

        WebElement generateFileButton = driver.findElement(By.id("create"));
        generateFileButton.click();

        WebElement downloadButton = driver.findElement(By.id("link-to-download"));
        downloadButton.click();

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loader")));

        File downloadedFolder = new File("C:\\Users\\s_zhm\\Downloads");
        File[] files = downloadedFolder.listFiles();

        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".txt")) {
                String fileText = readTextFromFile(file.getAbsolutePath());

                Assert.assertEquals(fileText, text);
            }
        }
        driver.quit();

    }

    public String readTextFromFile(String filePath) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
        }

        return stringBuilder.toString();
    }


}
