package org.zhmurko.tests.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class FramesTest {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("https://the-internet.herokuapp.com/nested_frames");
    }

    @DataProvider
    public Object[] dataProvider() {
        return new Object[][]{
                {"frame-left", "LEFT"},
                {"frame-middle", "MIDDLE"},
                {"frame-right", "RIGHT"},
                {"frame-bottom", "BOTTOM"}

        };
    }

    @Test(dataProvider = "dataProvider")
    public void frameInFrame(String frameName, String frameText) {
        if (!frameName.equals("frame-bottom")){
            driver.switchTo().frame("frame-top");
        }
        driver.switchTo().frame(frameName);

        String text = driver.findElement(By.tagName("body")).getText().trim();
        Assert.assertEquals(text, frameText);
    }

}
