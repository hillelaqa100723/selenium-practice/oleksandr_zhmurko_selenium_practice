package org.zhmurko.tests.base;

import driver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseClass {
    protected WebDriver driver;
    @BeforeSuite
    public void beforeClass() {
        driver = WebDriverFactory.initDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

}
